package com.example.tabtest;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> list;

    public ViewPagerAdapter(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
    }

    public void setList(List<Fragment> list) {
        this.list = list;
        notifyDataSetChanged(); // ????
    }


    public ViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
//        return null;
        return list.get(position);
    }

    @Override
    public int getCount() {
//        return 0;
        return list != null ? list.size() : 0;
    }
}
